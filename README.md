The code from this article: [GETTING STARTED WITH DRIZZLE AND REACT](https://www.trufflesuite.com/tutorials/getting-started-with-drizzle-and-react).

## Setting up the development environment

```sh
$ npm install -g truffle
```
## Get the code from [Bitbucket](https://bitbucket.org/yuriy_chistyakov/drizzl/)

```sh
$ git clone https://yuriy_chistyakov@bitbucket.org/yuriy_chistyakov/drizzl.git
```

## Steps to run the repo

```
$ cd drizzl
$ truffle develop
```

![truffle develop](https://res.cloudinary.com/dhcie5spu/image/upload/v1563136453/truffle_develop_s2spmt.png)

**Open another terminal and navigate to /client folder**

```
$ cd drizzl/client 
```

**Install dependencies**

```
$ yarn install
```

**Start client app**

```
$ yarn start
```

**Back to the first terminal and migrate the contract to the blockchain**

```
truffle(develop)> migrate
```

![migrate](https://res.cloudinary.com/dhcie5spu/image/upload/v1563136466/migrate_twyng1.png)

## Learn More

You can learn more in the [Truffle Suite documentation](https://www.trufflesuite.com/docs).

